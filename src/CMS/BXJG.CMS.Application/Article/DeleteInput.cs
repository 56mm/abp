﻿using BXJG.Utils.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.CMS.Article
{
    /// <summary>
    /// 批量删除时的输入模型
    /// </summary>
    public class DeleteInput: BulkOperationInput
    {
    }
}
