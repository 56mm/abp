﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.Utils.Enums
{
    /// <summary>
    /// 行政级别
    /// </summary>
    public enum AdministrativeLevel
    {
        /// <summary>
        /// 省、直辖市
        /// </summary>
        Pprovince,
        /// <summary>
        /// 市
        /// </summary>
        City,
        /// <summary>
        /// 区县
        /// </summary>
        County
    }
}
